# Instalación y configuración de php72 dentro de tu proyecto laravel 5.8

# Paso 1
Cambiar las variables con {} dentro de los archivos build/nginx.conf y el archivo docker-compose.yml
# Paso 2
Colocar el archivo nginx.conf dentro del proyecto laravel en una carpeta llamada "build" en la raíz del proyecto laravel
# Paso 3
Colocar el archivo docker-compose.yml y Dockerfile dentro de la raíz del proyecto laravel
# Paso 4
Ubicarse en la ruta del proyecto laravel dentro de la consola y ejecutar: $ docker-compose up -d
# Paso 5
Si tienes git, agregar al .gitignore:
build/nginx.conf
docker-compose.yml
Dockerfile